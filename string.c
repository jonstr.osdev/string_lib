#include "string.h"

#include "ctype.h"

#include <stddef.h>
#include <stdbool.h>


char *strcpy(char *dest, const char *src)
{
	char *tmp = dest;

	while ((*dest++ = *src++) != '\0') /* Copy src to dest */ ;

	return tmp;
}


char *strncpy(char *dest, const char *src, size_t count)
{
	char *tmp = dest;

	while (count) 
    {
		if ((*tmp = *src) != 0)
        {
			src++;
        }
		tmp++;
		count--;
	}

	return dest;
}


ssize_t strscpy(char *dest, const char *src, size_t count)
{
    size_t idx = 0;

    if (count == 0)
    {
        return -1;
    }

    while (idx < count - 1 && src[idx] != '\0')
    {
        dest[idx] = src[idx];
        idx++;
    }

    if (idx < count)
    {
        dest[idx] = '\0';
    }

    return idx < count ? (ssize_t)idx : -1;
}


char *strcat(char *dest, const char *src)
{
	char *tmp = dest;

	while (*dest)
    {
		dest++;
    }

	while ((*dest++ = *src++) != '\0') /* Copy src to dest */ ;

	return tmp;
}


char *strncat(char *dest, const char *src, size_t count)
{
	char *tmp = dest;

	if (count) 
    {
		while (*dest)
        {
			dest++;
        }

		while ((*dest++ = *src++) != 0)  /* Copy src to dest */
        {
			if (--count == 0) 
            {
				*dest = '\0';
				break;
			}
		}
	}

	return tmp;
}


size_t strlcat(char *dest, const char *src, size_t count)
{
	size_t dsize = strlen(dest);
	size_t len = strlen(src);
	size_t res = dsize + len;

	dest += dsize;
	count -= dsize;

	if (len >= count)
    {
		len = count-1;
    }

	memcpy(dest, src, len);
	dest[len] = 0;
	return res;
}


int strcmp(const char *cs, const char *ct)
{
	unsigned char c1, c2;

	while (true) 
    {
		c1 = *cs++;
		c2 = *ct++;
		
        if (c1 != c2)
        {
			return c1 < c2 ? -1 : 1;
        }

		if (!c1)
        {
			break;
        }
	}
	return 0;
}


int strncmp(const char *cs, const char *ct, size_t count)
{
	unsigned char c1, c2;

	while (count) 
    {
		c1 = *cs++;
		c2 = *ct++;
		
        if (c1 != c2)
        {
			return c1 < c2 ? -1 : 1;
        }

		if (!c1)
        {
			break;
        }

		count--;
	}

	return 0;
}


int strncasecmp(const char *s1, const char *s2, size_t len)
{
	unsigned char c1, c2;

	if (!len)
    {
		return 0;
    }

	do {
		c1 = *s1++;
		c2 = *s2++;
		if (!c1 || !c2)
        {
			break;
        }
		if (c1 == c2)
        {
			continue;
        }
		c1 = tolower(c1);
		c2 = tolower(c2);
		if (c1 != c2)
        {
			break;
        }
	} while (--len);

	return (int)c1 - (int)c2;
}


int strcasecmp(const char *s1, const char *s2)
{
	int c1, c2;

	do {
		c1 = tolower(*s1++);
		c2 = tolower(*s2++);
	} while (c1 == c2 && c1 != 0);
	return c1 - c2;
}


/* REVIST THIS IMPLEMENTATION */
char *strchr(const char *s, int c)
{
	for (; *s != (char)c; ++s)
    {
		if (*s == '\0')
        {
			return NULL;
        }
    }

	return (char *)s;
}


/* ALTERNATE */
// char *strrchr(const char *s, int c)
// {
// 	const char *last = NULL;
// 	do {
// 		if (*s == (char)c)
// 			last = s;
// 	} while (*s++);
// 	return (char *)last;
// }


char *strrchr(const char *s, int c)
{
	const char *last = NULL;
	do {
		if (*s == (char)c)
        {			
            last = s;
        }
	} while (*s++);

	return (char *)last;
}


size_t strspn(const char *s, const char *accept)
{
	const char *p;

	for (p = s; *p != '\0'; ++p) 
    {
		if (!strchr(accept, *p))
        {
			break;
        }
	}

	return p - s;
}


size_t strcspn(const char *s, const char *reject)
{
	const char *p;

	for (p = s; *p != '\0'; ++p) 
    {
		if (strchr(reject, *p))
        {
			break;
        }
	}

	return p - s;
}


char *strpbrk(const char *cs, const char *ct)
{
	const char *sc;

	for (sc = cs; *sc != '\0'; ++sc) 
    {
		if (strchr(ct, *sc))
        {
			return (char *)sc;
        }
	}
	return NULL;
}


char *strstr(const char *s1, const char *s2)
{
	size_t l1, l2;

	l2 = strlen(s2);
	if (!l2)
    {
		return (char *)s1;
    }
	l1 = strlen(s1);
	while (l1 >= l2) 
    {
		l1--;
		if (!memcmp(s1, s2, l2))
        {
			return (char *)s1;
        }
		s1++;
	}
	return NULL;
}


size_t strlen(const char *s)
{
	const char *sc;

	for (sc = s; *sc != '\0'; ++sc) /* inc sc until '\0'' */;
	return sc - s;
}


size_t strnlen(const char *s, size_t count)
{
	const char *sc;

	for (sc = s; count-- && *sc != '\0'; ++sc) /* dec count until '\0' */;
	return sc - s;
}


void *memcpy(void *dest, const void *src, size_t count)
{
	char *tmp = dest;
	const char *s = src;

	while (count--)
    {
		*tmp++ = *s++;
    }

	return dest;
}


void *memmove(void *dest, const void *src, size_t count)
{
	char *tmp;
	const char *s;

	if (dest <= src) 
    {
		tmp = dest;
		s = src;

		while (count--)
        {
			*tmp++ = *s++;
        }
	} 
    else 
    {
		tmp = dest;
		tmp += count;

		s = src;
		s += count;

		while (count--)
        {
			*--tmp = *--s;
        }
	}

	return dest;
}


int memcmp(const void *cs, const void *ct, size_t count)
{
	const unsigned char *su1, *su2;
	int res = 0;

	for (su1 = cs, su2 = ct; 0 < count; ++su1, ++su2, count--)
    {
		if ((res = *su1 - *su2) != 0)
        {
			break;
        }
    }

	return res;
}


void *memchr(const void *s, const char c, size_t n)
{
	const unsigned char *p = s;

	while (n-- != 0) 
    {
        if ((unsigned char)c == *p++) 
        {
			return (void *)(p - 1);
		}
	}

	return NULL;
}


void *memset(void *s, const char c, size_t count)
{
	char *xs = s;

	while (count--)
    {
		*xs++ = c;
    }

	return s;
}