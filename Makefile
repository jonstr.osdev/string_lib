CC=gcc
CFLAGS= -Og -g -std=c11 -ffreestanding -fno-builtin -no-pie

# Use wildcard to include all .c files in the root directory
SOURCES=$(wildcard *.c)
OBJECTS=$(SOURCES:.c=.o)

all: test

test: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o test

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f test $(OBJECTS)

run: test
	./test

debug: test
	gdb ./test
