#pragma once

/* Ripped from Linux */

#ifndef NULL
#define NULL ((void *)0)
#endif


#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned int size_t;
#endif


#ifndef _SSIZE_T
#define _SSIZE_T
typedef int ssize_t;
#endif


extern char *strcpy(char *dest, const char *src);
extern char *strncpy(char *dest, const char *src, size_t count);
extern ssize_t strscpy(char *dest, const char *src, size_t count);
extern char *strcat(char *dest, const char *src);
extern char *strncat(char *dest, const char *src, size_t count);
extern size_t strlcat(char *dest, const char *src, size_t count);
extern int strcmp(const char *cs, const char *ct);
extern int strncmp(const char *cs, const char *ct, size_t count);
extern int strncasecmp(const char *s1, const char *s2, size_t len);
int strcasecmp(const char *s1, const char *s2);
extern char *strchr(const char *s, int c);
extern char *strrchr(const char *s, int c);
extern size_t strspn(const char *s, const char *accept);
extern size_t strcspn(const char *s, const char *reject);
extern char *strpbrk(const char *cs, const char *ct);
extern char *strstr(const char *cs, const char *ct);
extern size_t strlen(const char *s);
extern size_t strnlen(const char *s, size_t count);
extern char *strtok(char *s, const char *ct);
extern void *memcpy(void *dest, const void *src, size_t n);
extern void *memmove(void *dest, const void *src, size_t n);
extern int memcmp(const void *cs, const void *ct, size_t count);
extern void *memchr(const void *cs, const char c, size_t count);
extern void *memset(void *s, const char c, size_t count);
