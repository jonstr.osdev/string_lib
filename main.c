#include "string.h"

#include <stdio.h>
#include <limits.h>
#include <stdbool.h>

static bool tests_failed = false;

void testResult(int condition, const char *message)
{
    if (!condition)
    {
        printf("%s: failed\n", message);
        tests_failed = true;
    }
}

int main(void)
{
    printf("Extended string.h Unit Tests\n");

    // strcpy tests
    char strcpy_dest[20];
    const char *strcpy_src = "Test strcpy";
    strcpy(strcpy_dest, strcpy_src);
    testResult(strcmp(strcpy_dest, strcpy_src) == 0, "strcpy");

    // strncpy tests
    char strncpy_dest[20];
    strncpy(strncpy_dest, "Hello strncpy", 5);
    strncpy_dest[5] = '\0';
    testResult(strcmp(strncpy_dest, "Hello") == 0, "strncpy normal");
    strncpy(strncpy_dest, "Short", 10);
    testResult(strcmp(strncpy_dest, "Short") == 0 && strncpy_dest[6] == '\0', "strncpy null padding");

    // strscpy tests
    char strscpy_dest[20];
    ssize_t scpy_ret = strscpy(strscpy_dest, "Test strscpy", sizeof(strscpy_dest));
    testResult(scpy_ret > 0 && strcmp(strscpy_dest, "Test strscpy") == 0, "strscpy");

    // strcat tests
    char strcat_dest[20] = "Hello ";
    strcat(strcat_dest, "World");
    testResult(strcmp(strcat_dest, "Hello World") == 0, "strcat");

    // strncat tests
    char strncat_dest[20] = "Hello ";
    strncat(strncat_dest, "World!!!", 5);
    testResult(strcmp(strncat_dest, "Hello World") == 0, "strncat");

    // strlcat tests
    char strlcat_dest[20] = "Hello";
    size_t lcat_ret = strlcat(strlcat_dest, " World", sizeof(strlcat_dest));
    testResult(strcmp(strlcat_dest, "Hello World") == 0 && lcat_ret == strlen("Hello World"), "strlcat");

    // strcmp tests
    testResult(strcmp("abc", "abc") == 0, "strcmp equal");
    testResult(strcmp("abc", "abd") < 0, "strcmp less than");
    testResult(strcmp("abd", "abc") > 0, "strcmp greater than");

    // strncmp tests
    testResult(strncmp("abc123", "abc456", 3) == 0, "strncmp equal");
    testResult(strncmp("abc123", "abc456", 6) < 0, "strncmp less than");
    testResult(strncmp("abc456", "abc123", 6) > 0, "strncmp greater than");

    // strncasecmp tests
    testResult(strncasecmp("ABC", "abc", 3) == 0, "strncasecmp equal");
    testResult(strncasecmp("abc", "ABD", 3) < 0, "strncasecmp less than");
    testResult(strncasecmp("ABD", "abc", 3) > 0, "strncasecmp greater than");

    // strchr tests
    const char *strchr_str = "Find a char";
    testResult(strchr(strchr_str, 'a') == strchr_str + 5, "strchr");

    // Enhanced strchr and strrchr tests
    // This test should locate the first 'a' in the string.
    const char *strchr_test_str = "Find the character: a char";
    testResult(strchr(strchr_test_str, 'a') == strchr_test_str + 11, "strchr first occurrence");
    testResult(strchr(strchr_test_str, 'z') == NULL, "strchr no occurrence");
    testResult(strrchr(strchr_test_str, 'a') == strchr_test_str + 24, "strrchr last occurrence");

    testResult(strrchr(strchr_test_str, 'z') == NULL, "strrchr no occurrence");

    // Enhanced strspn tests
    testResult(strspn("abcde12345", "abcde") == 5, "strspn all characters belong");
    testResult(strspn("12345abcde", "abcde") == 0, "strspn no characters belong");

    // Enhanced strcspn tests
    testResult(strcspn("abcde12345", "123") == 5, "strcspn until numbers");
    testResult(strcspn("12345abcde", "abcde") == 5, "strcspn until letters");

    // Enhanced strpbrk tests
    const char *strpbrk_test_str = "hello world";
    testResult(strpbrk("hello world", "a") == NULL, "strpbrk no common characters");
    testResult(strpbrk(strpbrk_test_str, "l") == strpbrk_test_str + 2, "strpbrk found common character");

    // Enhanced strstr tests
    // This test confirms that the substring "world" is found in "hello world".
    const char *strstr_test_str = "hello world";
    testResult(strstr(strstr_test_str, "world") == strstr_test_str + 6, "strstr found substring");
    testResult(strstr("hello world", "worlds") == NULL, "strstr no substring");

    // Enhanced strlen and strnlen tests
    testResult(strlen("hello world") == 11, "strlen");
    testResult(strnlen("hello world", 5) == 5, "strnlen with max smaller than length");
    testResult(strnlen("hello", 10) == 5, "strnlen with max larger than length");

    // Comprehensive strtok tests
    char strtok_str1[] = "token1,token2,token3";
    char *token = strtok(strtok_str1, ",");
    testResult(strcmp(token, "token1") == 0, "strtok first token");
    token = strtok(NULL, ",");
    testResult(strcmp(token, "token2") == 0, "strtok second token");
    token = strtok(NULL, ",");
    testResult(strcmp(token, "token3") == 0, "strtok third token");
    token = strtok(NULL, ",");
    testResult(token == NULL, "strtok no more tokens");

    // Comprehensive memcpy tests
    char memcpy_dest3[20];
    const char *memcpy_src3 = "Copy this string!";
    memcpy(memcpy_dest3, memcpy_src3, 17);
    testResult(strcmp(memcpy_dest3, memcpy_src3) == 0, "memcpy with exact size");

    // Test memmove
// Test memmove with non-overlapping regions
    char memmove_str1[] = "Destination";
    memmove(memmove_str1, "Source", 7);
    testResult(strcmp(memmove_str1, "Source") == 0, "memmove with non-overlapping");

    // Test memmove with overlapping regions
    char memmove_str2[] = "Destination";
    memmove(memmove_str2 + 3, memmove_str2, 7);
    testResult(strcmp(memmove_str2, "DesDestinan") == 0, "memmove with overlapping");


    // Comprehensive memcmp tests
    testResult(memcmp("1234", "1234", 4) == 0, "memcmp identical strings");
    testResult(memcmp("1234", "1235", 4) < 0, "memcmp string1 less than string2");
    testResult(memcmp("1235", "1234", 4) > 0, "memcmp string1 greater than string2");
    testResult(memcmp("1234", "1234", 0) == 0, "memcmp zero-length strings");

    // Comprehensive memchr tests
    const char *memchr_str2 = "abcde";
    testResult(memchr(memchr_str2, 'a', 5) == memchr_str2, "memchr find first char");
    testResult(memchr(memchr_str2, 'e', 5) == memchr_str2 + 4, "memchr find last char");
    testResult(memchr(memchr_str2, 'z', 5) == NULL, "memchr char not found");

    // Comprehensive memset tests
    char memset_str2[6];
    memset(memset_str2, 'A', 5);
    memset_str2[5] = '\0';
    testResult(strcmp(memset_str2, "AAAAA") == 0, "memset set all chars");
    memset(memset_str2, '\0', 5);
    testResult(memset_str2[0] == '\0' && memset_str2[4] == '\0', "memset set to null chars");

    if (tests_failed)
    {
        printf("Some tests failed\n");
    }
    else
    {
        printf("All tests passed\n");
    }

    return 0;
}
